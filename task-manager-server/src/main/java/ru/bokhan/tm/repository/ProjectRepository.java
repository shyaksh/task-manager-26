package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.IProjectRepository;
import ru.bokhan.tm.dto.ProjectDTO;
import ru.bokhan.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<ProjectDTO> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager em) {
        super(em);
    }

    @Override
    public @NotNull List<ProjectDTO> findAll() {
        return em.createQuery("SELECT e FROM ProjectDTO e", ProjectDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull String id) {
        return em.createQuery("SELECT e FROM ProjectDTO e WHERE e.id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public long count() {
        return em.createQuery("SELECT COUNT(e) FROM ProjectDTO e", Long.class).getSingleResult();
    }


    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<ProjectDTO> list = em.createQuery(
                "SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class
        )
                .setParameter("userId", userId)
                .getResultList();
        list.forEach(this::remove);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        return em.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull final String userId, @NotNull final String id) {
        return em.createQuery("SELECT e FROM ProjectDTO e WHERE e.id = :id AND e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public ProjectDTO findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<ProjectDTO> list = em.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
        if (index > list.size()) return null;
        return list.get(index);
    }

    @Nullable
    @Override
    public ProjectDTO findByName(@NotNull final String userId, @NotNull final String name) {
        return em.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final ProjectDTO projectDTO = findById(userId, id);
        if (projectDTO == null) return;
        remove(projectDTO);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO projectDTO = findByIndex(userId, index);
        if (projectDTO == null) return;
        remove(projectDTO);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final ProjectDTO projectDTO = findByName(userId, name);
        if (projectDTO == null) return;
        remove(projectDTO);
    }

    @Override
    public void remove(@NotNull final ProjectDTO dto) {
        @Nullable final Project project = em.createQuery("SELECT e FROM Project e WHERE e.id = :id", Project.class)
                .setParameter("id", dto.getId())
                .getSingleResult();
        if (project == null) return;
        em.remove(project);
    }

}