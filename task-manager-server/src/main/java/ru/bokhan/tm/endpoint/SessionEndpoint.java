package ru.bokhan.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IServiceLocator;
import ru.bokhan.tm.api.endpoint.ISessionEndpoint;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.empty.EmptyLoginException;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class SessionEndpoint implements ISessionEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().close(session);
    }

    @Override
    @WebMethod
    public void closeSessionAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().closeAll(session);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO getUserBySession(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getSessionService().getUser(session);
    }

    @Override
    @NotNull
    @WebMethod
    public String getUserIdBySession(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getSessionService().getUserId(session);
    }

    @Override
    @NotNull
    @WebMethod
    public List<SessionDTO> findSessionAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getSessionService().findAll(session);
    }

    @Override
    @Nullable
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) {
        if (login == null) throw new EmptyLoginException();
        if (password == null) throw new EmptyLoginException();
        return serviceLocator.getSessionService().open(login, password);
    }

    @Override
    @WebMethod
    public void signOutByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().signOutByLogin(login);
    }

    @Override
    @WebMethod
    public void signOutByUserId(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "userId") @Nullable String userId
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().signOutByUserId(userId);
    }

    @Override
    @WebMethod
    public void removeSessionAll(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().clear();
    }

    @Override
    @WebMethod
    public void removeSession(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().remove(session);
    }

}
