package ru.bokhan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.endpoint.*;

public interface IEndpointLocator {

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    DomainEndpoint getDomainEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @Nullable
    SessionDTO getCurrentSession();

    void setCurrentSession(@Nullable SessionDTO session);

}
