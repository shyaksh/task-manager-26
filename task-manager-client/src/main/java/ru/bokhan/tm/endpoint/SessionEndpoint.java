package ru.bokhan.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-26T20:52:57.240+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.bokhan.ru/", name = "SessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface SessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/getUserBySessionRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/getUserBySessionResponse")
    @RequestWrapper(localName = "getUserBySession", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.GetUserBySession")
    @ResponseWrapper(localName = "getUserBySessionResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.GetUserBySessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bokhan.tm.endpoint.UserDTO getUserBySession(
        @WebParam(name = "session", targetNamespace = "")
        ru.bokhan.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/signOutByUserIdRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/signOutByUserIdResponse")
    @RequestWrapper(localName = "signOutByUserId", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.SignOutByUserId")
    @ResponseWrapper(localName = "signOutByUserIdResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.SignOutByUserIdResponse")
    public void signOutByUserId(
        @WebParam(name = "session", targetNamespace = "")
        ru.bokhan.tm.endpoint.SessionDTO session,
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/findSessionAllRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/findSessionAllResponse")
    @RequestWrapper(localName = "findSessionAll", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.FindSessionAll")
    @ResponseWrapper(localName = "findSessionAllResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.FindSessionAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.bokhan.tm.endpoint.SessionDTO> findSessionAll(
        @WebParam(name = "session", targetNamespace = "")
        ru.bokhan.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/removeSessionRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/removeSessionResponse")
    @RequestWrapper(localName = "removeSession", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.RemoveSession")
    @ResponseWrapper(localName = "removeSessionResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.RemoveSessionResponse")
    public void removeSession(
        @WebParam(name = "session", targetNamespace = "")
        ru.bokhan.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/closeSessionAllRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/closeSessionAllResponse")
    @RequestWrapper(localName = "closeSessionAll", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.CloseSessionAll")
    @ResponseWrapper(localName = "closeSessionAllResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.CloseSessionAllResponse")
    public void closeSessionAll(
        @WebParam(name = "session", targetNamespace = "")
        ru.bokhan.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/signOutByLoginRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/signOutByLoginResponse")
    @RequestWrapper(localName = "signOutByLogin", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.SignOutByLogin")
    @ResponseWrapper(localName = "signOutByLoginResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.SignOutByLoginResponse")
    public void signOutByLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.bokhan.tm.endpoint.SessionDTO session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/removeSessionAllRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/removeSessionAllResponse")
    @RequestWrapper(localName = "removeSessionAll", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.RemoveSessionAll")
    @ResponseWrapper(localName = "removeSessionAllResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.RemoveSessionAllResponse")
    public void removeSessionAll(
        @WebParam(name = "session", targetNamespace = "")
        ru.bokhan.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/closeSessionRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/closeSessionResponse")
    @RequestWrapper(localName = "closeSession", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.CloseSession")
    @ResponseWrapper(localName = "closeSessionResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.CloseSessionResponse")
    public void closeSession(
        @WebParam(name = "session", targetNamespace = "")
        ru.bokhan.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/getUserIdBySessionRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/getUserIdBySessionResponse")
    @RequestWrapper(localName = "getUserIdBySession", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.GetUserIdBySession")
    @ResponseWrapper(localName = "getUserIdBySessionResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.GetUserIdBySessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String getUserIdBySession(
        @WebParam(name = "session", targetNamespace = "")
        ru.bokhan.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.bokhan.ru/SessionEndpoint/openSessionRequest", output = "http://endpoint.tm.bokhan.ru/SessionEndpoint/openSessionResponse")
    @RequestWrapper(localName = "openSession", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.OpenSession")
    @ResponseWrapper(localName = "openSessionResponse", targetNamespace = "http://endpoint.tm.bokhan.ru/", className = "ru.bokhan.tm.endpoint.OpenSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.bokhan.tm.endpoint.SessionDTO openSession(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );
}
